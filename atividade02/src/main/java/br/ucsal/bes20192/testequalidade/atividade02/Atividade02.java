package br.ucsal.bes20192.testequalidade.atividade02;

import java.util.Scanner;

/*
Construa um programa em Java que realize as seguintes atividades:
Obtenha um n�mero entre 1 e 10000, intervalo fechado.
Verifique se o n�mero informado � ou n�o primo.
Apresente o resultado atrav�s da mensagem: �O n�mero <n�mero> � primo.� ou �O n�mero <n�mero> n�o � primo.�, conforme o caso.

 */
public class Atividade02 {

	private static final int NUM_MIN = 1;
	private static final int NUM_MAX = 10000;

	public static void main(String[] args) {
		Integer n;
		Boolean ehPrimo;

		n = obterNumero();
		ehPrimo = verificarEhPrimo(n);
		exibirTipoNumero(n, ehPrimo);
	}

	public static Integer obterNumero() {
		Scanner scanner = new Scanner(System.in);
		Integer n;
		while (true) {
			System.out.println("Informe um n�mero entre 1 e 10000 (intervalo fechado):");
			n = scanner.nextInt();
			try {
				validarNumero(n);
				return n;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static void validarNumero(Integer n) throws NumeroInvalidoException {
		if (n < NUM_MIN || n > NUM_MAX) {
			throw new NumeroInvalidoException("N�mero fora da faixa");
		}
	}

	public static Boolean verificarEhPrimo(Integer n) {
		Integer qtdDivisores = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				qtdDivisores++;
			}
		}
		if (qtdDivisores == 2) {
			return true;
		}
		return false;
	}

	public static void exibirTipoNumero(Integer n, Boolean ehPrimo) {
		String descricaoTipoNumero = definirDescricaoPrimo(ehPrimo);
		System.out.println("O n�mero " + n + descricaoTipoNumero + ".");
	}

	private static String definirDescricaoPrimo(Boolean ehPrimo) {
		if (ehPrimo) {
			return " � primo";
		}
		return " n�o � primo";
	}

}
