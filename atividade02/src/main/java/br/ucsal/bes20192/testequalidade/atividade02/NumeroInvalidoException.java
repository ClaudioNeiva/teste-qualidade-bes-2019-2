package br.ucsal.bes20192.testequalidade.atividade02;

public class NumeroInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public NumeroInvalidoException(String message) {
		super(message);
	}

}
