package br.ucsal.bes20192.testequalidade.atividade02;

import java.io.ByteArrayInputStream;

import org.junit.Assert;
import org.junit.Test;

public class Atividade02EntradaDadosTest {

	@Test
	public void testarEntradaValida() {
		// Definir dados de entrada
		String nEntrada = "20";

		// Enquanto n�o estuda frameworks de mock, podemos utilizar uma solu��o t�cnica
		// de baixo impacto (gambi) pra resolver o problema da entrada de dados do
		// Scanner.
		ByteArrayInputStream inputFake = new ByteArrayInputStream(nEntrada.getBytes());
		System.setIn(inputFake);
			
		// Defini sa�da esperada
		Integer nEsperado = 20;

		// Executar m�todo que est� sendo testado e obter a sa�da atual
		Integer nAtual = Atividade02.obterNumero();

		// Comparar a sa�da esperada com a sa�da atual
		Assert.assertEquals(nEsperado, nAtual);
	}

	@Test
	public void testarEntrada1ValorNaoValido() {
		// Definir dados de entrada
		String nEntrada = "0\n30000\n20";

		// Enquanto n�o estuda frameworks de mock, podemos utilizar uma solu��o t�cnica
		// de baixo impacto (gambi) pra resolver o problema da entrada de dados do
		// Scanner.
		ByteArrayInputStream inputFake = new ByteArrayInputStream(nEntrada.getBytes());
		System.setIn(inputFake);
			
		// Defini sa�da esperada
		Integer nEsperado = 20;

		// Executar m�todo que est� sendo testado e obter a sa�da atual
		Integer nAtual = Atividade02.obterNumero();

		// Comparar a sa�da esperada com a sa�da atual
		Assert.assertEquals(nEsperado, nAtual);
	}
	
	@Test
	public void testarValidacaoValido() {
		Integer n = 1;
		try {
			Atividade02.validarNumero(n);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test(expected = Exception.class)
	public void testarValidacaoInvalido() throws Exception {
		Integer n = -1;
		Atividade02.validarNumero(n);
	}
}
