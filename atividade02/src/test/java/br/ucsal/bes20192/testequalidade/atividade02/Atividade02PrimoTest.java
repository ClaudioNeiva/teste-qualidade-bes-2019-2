package br.ucsal.bes20192.testequalidade.atividade02;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Atividade02PrimoTest {

	@Parameters(name = "verificar se {0} � primo = {1}")
	public static Collection<Object[]> definirDadosTeste() {
		// Collection<Object[]> colecao = new ArrayList<Object[]>();
		// colecao.add(new Object[] { 5, true });
		// colecao.add(new Object[] { 7, true });
		// colecao.add(new Object[] { 8, false });
		// colecao.add(new Object[] { 10, false });
		// return colecao;
		return Arrays.asList(new Object[][] { { 5, true }, { 7, true }, { 8, false }, { 10, false }, { 15, false },
				{ 19, true }, { 30, false }, { 1000, false }, { 3000, false }, { 3301, true } });
	}

	@Parameter // o zero � default, ou seja, � a mesma coisa de @Parameter(0)
	public Integer n;

	@Parameter(1)
	public Boolean ehPrimoEsperado;

	@Test
	public void testarEhPrimo() {
		// Executar o m�todo que est� sendo testado e coleta o resultado atual
		Boolean ehPrimoAtual = Atividade02.verificarEhPrimo(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(ehPrimoEsperado, ehPrimoAtual);
	}
	
}
