package br.ucsal.bes20192.testequalidade.aula09;

public class CalculoTUI {

	public static void main(String[] args) {
		FatorialHelper fatorialHelper = new FatorialHelper();
		CalculoEHelper calculoEHelper = new CalculoEHelper(fatorialHelper);
		System.out.println(calculoEHelper.calcularE(3));
	}

}
