package br.ucsal.bes20192.testequalidade.aula09;

public class FatorialHelper {

	public Long calcularFatorial(Integer n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
