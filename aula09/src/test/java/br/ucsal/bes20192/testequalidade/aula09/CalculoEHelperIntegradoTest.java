package br.ucsal.bes20192.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculoEHelperIntegradoTest {

	private FatorialHelper fatorialHelper;

	private CalculoEHelper calculoEHelper;

	@Before
	public void setup() {
		fatorialHelper = new FatorialHelper();
		calculoEHelper = new CalculoEHelper(fatorialHelper);
	}

	@Test
	public void testarCalculoE2() {
		Integer n = 2;
		Double eEsperado = 2.5; // 2.51 2.49
		Double eAtual = calculoEHelper.calcularE(n);
		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
