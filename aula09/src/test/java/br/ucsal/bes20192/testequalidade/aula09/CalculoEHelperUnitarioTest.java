package br.ucsal.bes20192.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class CalculoEHelperUnitarioTest {

	private FatorialHelper fatorialHelperMock;

	private CalculoEHelper calculoEHelper;

	@Before
	public void setup() {
		// Esta � a forma mais b�sica para criar objetos mocados.
		// Teste o uso da annotation @Mock com o uso do initMocks
		// e o uso de um runner no lugar do initMocks
		fatorialHelperMock = Mockito.mock(FatorialHelper.class);
		calculoEHelper = new CalculoEHelper(fatorialHelperMock);
	}

	@Test
	public void testarCalculoE2() {
		Integer n = 2;
		Double eEsperado = 2.5; // 2.51 2.49

		Mockito.when(fatorialHelperMock.calcularFatorial(0)).thenReturn(1L);
		Mockito.when(fatorialHelperMock.calcularFatorial(1)).thenReturn(1L);
		Mockito.when(fatorialHelperMock.calcularFatorial(2)).thenReturn(2L);

		Double eAtual = calculoEHelper.calcularE(n);
		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
