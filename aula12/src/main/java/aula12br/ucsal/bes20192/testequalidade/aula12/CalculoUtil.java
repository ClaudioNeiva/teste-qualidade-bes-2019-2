package aula12br.ucsal.bes20192.testequalidade.aula12;

public class CalculoUtil {

	public static Integer calculaMDC(Integer n1, Integer n2) {
		Integer mdc = 1;
		Integer menor = Math.min(n1, n2);
		for (int i = 1; i <= menor; i++) {
			if ((n1 % i == 0) && (n2 % i == 0)) {
				mdc = i;
			}
		}
		return mdc;
	}

}
