package aula12br.ucsal.bes20192.testequalidade.aula12;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	@Test
	public void testarMDC15e9() {

		Integer n1 = 15;
		Integer n2 = 9;

		Integer mdcEsperado = 3;

		Integer mdcAtual = CalculoUtil.calculaMDC(n1, n2);

		Assert.assertEquals(mdcEsperado, mdcAtual);

	}

	@Test
	public void testarMDC9e2() {

		Integer n1 = 9;
		Integer n2 = 3;

		Integer mdcEsperado = 3;

		Integer mdcAtual = CalculoUtil.calculaMDC(n1, n2);

		Assert.assertEquals(mdcEsperado, mdcAtual);

	}

}
