package aula12br.ucsal.bes20192.testequalidade.aula13;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class UCSalFirefoxTest {

	private static WebDriver driver;

	@BeforeClass
	public static void setup() {
		System.setProperty("webdriver.gecko.driver", "path/geckodriver.exe");
		System.setProperty("webdriver.firefox.bin", "C:\\Program Files\\Mozilla Firefox\\firefox.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		driver = (WebDriver) new FirefoxDriver(capabilities);

	}

	@AfterClass
	public static void tearDown() {
		driver.quit();
	}

	@Test
	public void testeUCSal01() {
		// Abrir p�gina da UCSal
		driver.get("http://www.ucsal.br");
		// Clicar no link Gradua��o
		WebElement graduacaoLink = driver.findElement(By.linkText("GRADUA��O"));
		graduacaoLink.click();
		// Preencher o input de pesquisa
		WebElement queryInput = driver.findElement(By.name("query"));
		queryInput.sendKeys("teste qualidade");
		// Submeter o form:
		queryInput.submit();

		// Obter o conte�do da p�gina
		String conteudo = driver.getPageSource();
		// Verificar se o texto �Teste Qualidade CST 2016-2� est� na p�gina
		Assert.assertTrue(conteudo.contains("Teste Qualidade CST 2016-2"));

	}

}
